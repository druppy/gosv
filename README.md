# Go service 

When setting up a modern service orientated backend, there are to parts found in that code, the domain specific code that is special for this given service and then the commen parts, often shared by other services.

All services are modeled around remote functions and CRUD like entity model, that all will be accessable using JsonRPC, RESTful or GraphQL. This makes it easy to create services that can use multible client with diferant needs or even a client that user all in combination.

We use access control on all registred element to make sure that only users with proper access are allowed to do requested actions.

JsonRPC 2.0 is simple to use, and well supported, and with [OpenRPC](https://open-rpc.org), it has never been better defined.

RESTful is really good for listing and paging, and allows good use of the http caching found in most modern browsers.

[GraphQL](https://graphql.org) allows for well defined metadata, gives us a type system, and makes composed requests possible, and combined with rest calles we can have the best of both worlds.

# Context

The services will all use the go Context for all elements that needs this, in order to get session or tenant specific data. 

Only the middle layer elements sets these up, and can easily be replaced.

There is a session based middle layer, out of the box, but if bearer token or other is needed, this wil only demand for a new middle layer the provice this.

# Source

The source code are structured into the following sub modules, and the startup will be possible using a normal standalone http service, or as a Google appengine.

We use go 1.11+ modules !

## Service

This contain the base service, for starting up and handling things like sessions and other elements. 
Sessions are secure sessions, that carry information about the user and the login validity, the session store are mostly a cache of things we can find in the DB. We will therfor not need at session store, but kan just keep things in memory.

We also define common session definitions, for the rest of the code base.

Defines a simple access model, that will define the access for all users and data in the system.

The service code also holds a simple event handling mecanism to allow for intercommunication. 

We also setup server send event (SSE) and prepare for more HTTP/2 like pushbacks.

## Entity

To handle the common senario of having an CRUD data table like flow, the entity defines this, by providing interface to define and register these entities.

This contains the basic entity registering and definitions. Defines the column types, query argument types and dependencies.

To make an entity we need to create a definition struture and register this to the gosv entity registry.

This is the most simple setup for an entity. The `myEntity` struture will register using a column structure, and a set of possible arguments to provide as much meta data as possible, in order to make introspection and validation as rich as possible.

```go
type cols struct {
    Name string
    Email string
}

type myEntity struct {}

// fulfill Query interface
func (e *myEntity) Get(ctx context.Context, key interface{}) (interface{}, error) {
    return cols{ 
        Name: "test",
        Email: "test@example.com",
    }, nil
}

func (e *myEntity) Query(ctx context.Context, args map[string]interface{}, order []string, limit int32, offset uint64) (entity.QueryResult, error) {
    return nil, fmt.Error( "not implemented" )
}

entity.Reg( "my_entity", Definition{
    Cols: reflect.TypeOf((*cols)(nil)),
    Args: []e.Arg{
		{Name: "name", Type: reflect.TypeOf((*string)(nil))},
    },
    Def: myEntity{}
})
```

If `myEntity` also fulfill the `entity.Updateable` interface we will support the full set of REST instructions.

When this is done, the entity is accessable as both RESTful api, rpc and GraphQL, and the framework is as native go as possible, on the same time its multi protocol.

### Struct tags

When defining structures, it is possible to add some struct tags to control how gosv handles the value mapping.

| Tag      | Value    | Description |
| ---      | ---      | --- |
| desc     | text     | If the protocol can result in any kind of description |
| fieldtag | noupdate | do not allow for update of this field |

## Entity builder

This is a compromise where we are building upon an SQL builder, and by that makes it much more easy to build database relations.

Fulfill the `QueryBuilder` interface, and use `ReqQuery` and the it is only necessary to return builder conditions to allow for filtering, the rest are done implicitly. 

## JsonRPC 2.0

This simple RPC interface is easy to use, and using the [OpenRPC](https://spec.open-rpc.org) to define the API.

To export a function in go as a RPC function, it only need to register this along with a name. All arguments will be mapped to the function, and can use normal Json mashalling, if any special conversion is needed. The arguments must be values, not interface definitions nor pointers. If argument types are not valid, the `rpc.Reg` function will panic.

```go
rpc.Reg( "my_rpc", func( name string, age int ) boolean { return false })
```

The first argument may be an `context.Context`, and if so this will *NOT* be provided by the RPC but given by the rpc http handler, for context relavant request data. The handler will only pass this along, but are not using it.

## RESTful

The router mapping and handling for the rest service to the entities.

Defining is using Swagger [OpenAPI](https://swagger.io/specification) to let relect the intern API.

## GraphQL

Maps GraphQL to both the entity and the RPC function to make it possible to access all these for graphql too.