// Handle incomming restful requests, and map these to the relevant entity definition

package restful

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strconv"

	"github.com/go-chi/chi"
	"gitlab.com/druppy/gosv/entity"
	"gitlab.com/druppy/gosv/rpc"
)

func httpJSON(w http.ResponseWriter, d interface{}) {
	json.NewEncoder(w).Encode(d)
}

func httpError(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), 500)
}

// Capture returned error
func handler(fn func(w http.ResponseWriter, r *http.Request) error) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := fn(w, r); err != nil {
			httpError(w, err)
		}
	}
}

var rangeRegex = regexp.MustCompile("items=(\\d+)-(\\d*)")

func entityGet(ctx context.Context, entityName string, key interface{}) (interface{}, error) {
	def, err := entity.Find(entityName)
	if err != nil {
		return nil, err
	}

	res, err := def.Def.Get(ctx, key)
	if err != nil {
		return res, err
	}

	return nil, err
}

func entityQuery(ctx context.Context, entityName string, args map[string]interface{}, order []string, limit int, offset int) ([]interface{}, error) {
	var def *entity.Definition
	var err error

	if def, err = entity.Find(entityName); err != nil {
		return nil, err
	}

	// Finally call the entity
	res, err := def.Def.Query(ctx, args, order, limit, offset)
	if err != nil {
		return nil, err
	}

	// Note this need to be more streaming like, but for now this will work
	var cont []interface{}
	for res.Next() {
		row, err := res.Row()

		if err != nil {
			return nil, err
		}

		cont = append(cont, row)
	}

	return cont, nil
}

func entityCreate(ctx context.Context, entityName string, data map[string]interface{}) (interface{}, error) {
	def, err := entity.Find(entityName)
	if err != nil {
		return nil, err
	}

	// get interface as upgradable
	u, ok := def.Def.(entity.Updateable)
	if !ok {
		return nil, fmt.Errorf("entity is not updatable, can't create new")
	}

	res, err := u.Create(ctx, data)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func entityUpdate(ctx context.Context, entityName string, key interface{}, data map[string]interface{}) (interface{}, error) {
	def, err := entity.Find(entityName)
	if err != nil {
		return nil, err
	}

	u, ok := def.Def.(entity.Updateable)
	if !ok {
		return nil, fmt.Errorf("entity is not updatable, can't update")
	}

	res := u.Update(ctx, key, data)

	return res, nil
}

func entityDelete(ctx context.Context, entityName string, key interface{}) (interface{}, error) {
	def, err := entity.Find(entityName)
	if err != nil {
		return nil, err
	}

	u, ok := def.Def.(entity.Updateable)
	if !ok {
		return nil, fmt.Errorf("entity is not updatable, can't delete")
	}

	return u.Delete(ctx, key), nil
}

// Setup restful handling using gin
func Setup(r chi.Router) {
	// Handle a single entity
	r.Get("/{entity_name}/{key}", handler(func(w http.ResponseWriter, r *http.Request) error {
		res, err := entityGet(r.Context(), chi.URLParam(r, "entity_name"), chi.URLParam(r, "key"))
		if err != nil {
			return err
		}
		httpJSON(w, res)
		return nil
	}))

	// Handle queries of entity
	r.Get("/{entity_name}", handler(func(w http.ResponseWriter, r *http.Request) error {
		var offset int
		var limit int = -1
		query := r.URL.Query()
		order := []string{}

		// respect the order given on query
		if query.Get("order") != "" {
			order = query["order"]
			query.Del("order")
		}

		// look for http Range header and convert this this limit / offset
		if r := r.Header.Get("Range"); len(r) > 0 {
			m := rangeRegex.FindAllString(r, 2)

			if len(m) == 2 {
				if from, err := strconv.ParseInt(m[0], 10, 64); err == nil {
					if to, err := strconv.ParseInt(m[1], 10, 64); err == nil && to > from {
						offset = int(from)
						limit = int(to - from)
					}
				}
			}
		}

		args := make(map[string]interface{})
		for k := range query {
			args[k] = query.Get(k)
		}

		res, err := entityQuery(r.Context(), chi.URLParam(r, "entity_name"), args, order, limit, offset)
		if err != nil {
			return err
		}
		httpJSON(w, res)

		return nil
	}))

	r.Post("/{entity_name}", handler(func(w http.ResponseWriter, r *http.Request) error {
		// Read the body
		decoder := json.NewDecoder(r.Body)
		var args map[string]interface{}
		if err := decoder.Decode(&args); err != nil {
			return err
		}

		res, err := entityCreate(r.Context(), chi.URLParam(r, "entity_name"), args)
		if err != nil {
			return err
		}

		httpJSON(w, res)
		return nil
	}))

	r.Put("/{entity_name}/{key}", handler(func(w http.ResponseWriter, r *http.Request) error {
		// Read the body
		decoder := json.NewDecoder(r.Body)
		var args map[string]interface{}
		if err := decoder.Decode(&args); err != nil {
			return err
		}

		res, err := entityUpdate(r.Context(), chi.URLParam(r, "entity_name"), chi.URLParam(r, "key"), args)
		if err != nil {
			return err
		}

		httpJSON(w, res)
		return nil
	}))

	r.Delete("/{entity_name}/{key}", handler(func(w http.ResponseWriter, r *http.Request) error {
		res, err := entityDelete(r.Context(), chi.URLParam(r, "entity_name"), chi.URLParam(r, "key"))

		if err != nil {
			return err
		}
		httpJSON(w, res)
		return nil
	}))

	// add RPC version of the entity access system too
	rpc.Reg("entity.get", func(ctx context.Context, entityName string, key interface{}) (interface{}, error) {
		return entityGet(ctx, entityName, key)
	})

	rpc.Reg("entity.query", func(ctx context.Context, entityName string, args map[string]interface{}, order []string, limit int, offset int) ([]interface{}, error) {
		return entityQuery(ctx, entityName, args, order, limit, offset)
	})

	rpc.Reg("entity.update", func(ctx context.Context, entityName string, key interface{}, data map[string]interface{}) (interface{}, error) {
		return entityUpdate(ctx, entityName, key, data)
	})

	rpc.Reg("entity.create", func(ctx context.Context, entityName string, data map[string]interface{}) (interface{}, error) {
		return entityCreate(ctx, entityName, data)
	})

	rpc.Reg("entity.delete", func(ctx context.Context, entityName string, key interface{}) (interface{}, error) {
		return entityDelete(ctx, entityName, key)
	})
}
