package graphql

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/go-chi/chi"

	"database/sql/driver"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
	"gitlab.com/druppy/gosv/entity"
	"gitlab.com/druppy/gosv/service"

	"github.com/rs/zerolog/log"
)

// getType converts reflect primitive types to graphql types
func getType(t reflect.Type) (*graphql.Scalar, error) {
	switch t.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int32, reflect.Int64:
		return graphql.Int, nil

	case reflect.Uint, reflect.Uint8, reflect.Uint32, reflect.Uint64:
		return graphql.Int, nil

	case reflect.Float32, reflect.Float64:
		return graphql.Float, nil

	case reflect.String:
		return graphql.String, nil

	case reflect.Bool:
		return graphql.Boolean, nil

	case reflect.Ptr:
		return getType(t.Elem())

	case reflect.Struct:
		name := t.Name()

		switch name {
		case "Time":
			return graphql.DateTime, nil
		case "NullString":
			return NullableString, nil
		case "NullTime":
			return NullableTime, nil
		case "NullInt64":
			return NullableInt64, nil
		}
	}

	return nil, fmt.Errorf("unknown type '%s' in entity structs", t.Name())
}

func makeSearchArguments(def *entity.Definition) graphql.FieldConfigArgument {
	args := graphql.FieldConfigArgument{}

	for _, arg := range def.Args {
		if t, err := getType(arg.Type); err == nil {
			args[arg.Name] = &graphql.ArgumentConfig{
				Type:        t,
				Description: arg.Desc,
			}
		} else {
			log.Panic().
				Str("error", err.Error()).
				Str("argument name", arg.Name).
				Msg("error building entity arguments")
		}
	}

	return args
}

func contains(elms []string, key string) bool {
	for _, e := range elms {
		if e == key {
			return true
		}
	}
	return false
}

func makeUpdateFields(def *entity.Definition) graphql.InputObjectConfigFieldMap {
	args := graphql.InputObjectConfigFieldMap{}

	t := def.Cols

	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	for i := 0; i < t.NumField(); i++ {
		finfo := t.Field(i)

		name := finfo.Name

		desc := ""

		if elms := strings.Split(finfo.Tag.Get("desc"), ","); len(elms) > 0 {
			desc = elms[0]
		}

		if elms := strings.Split(finfo.Tag.Get("fieldtag"), ","); len(elms) > 0 {
			if !contains(elms, "noupdate") {
				if t, err := getType(finfo.Type); err == nil {
					args[name] = &graphql.InputObjectFieldConfig{
						Description: desc,
						Type:        t,
					}
				}
			}
		}
	}

	return args
}

var objectTypes map[string]*graphql.Object = make(map[string]*graphql.Object)

func makeQueryType(name string) (*graphql.Object, error) {
	if obj, ok := objectTypes[name]; ok {
		return obj, nil
	}

	log.Debug().Msgf("make object type '%s'", name)
	def, err := entity.Find(name)
	if err != nil {
		return nil, err
	}

	flds := makeQueryFields(name, def)

	objectTypes[name] = graphql.NewObject(graphql.ObjectConfig{
		Name:   name,
		Fields: flds,
	})

	return objectTypes[name], nil
}

func makeQueryFields(name string, def *entity.Definition) graphql.Fields {
	flds := graphql.Fields{}

	ct := def.Cols

	if ct.Kind() == reflect.Ptr {
		ct = def.Cols.Elem()
	}

	// Add all fields defined by the entity
	for i := 0; i < ct.NumField(); i++ {
		finfo := ct.Field(i)

		// either use lower case name or use the db tag for override
		name := finfo.Name

		desc := ""
		if elms := strings.Split(finfo.Tag.Get("desc"), ","); len(elms) > 0 && len(elms[0]) > 0 {
			desc = elms[0]
		}

		if t, err := getType(finfo.Type); err == nil {
			flds[name] = &graphql.Field{
				Name:        name,
				Type:        t,
				Description: desc,
				/*Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					v := reflect.ValueOf(p.Source)

					if v.Kind() == reflect.Ptr {
						v = v.Elem()
					}

					if v.Type() == ct {
						return v.FieldByName(finfo.Name).Interface(), nil
					}

					return nil, fmt.Errorf("graphql can't resolve field %s from structure", finfo.Name)
				},*/
			}
		} else {
			log.Panic().
				Str("err", err.Error()).
				Msg("init type setup error")
		}
	}

	// Add all related entities
	for _, d := range def.Deps {
		var retType graphql.Output

		dep := d // capture for the function closure !
		retType, _ = makeQueryType(dep.Name)

		// is this 1:n relation ?
		if len(dep.ArgName) > 0 {
			retType = graphql.NewList(retType)
		}

		flds[dep.Name] = &graphql.Field{
			Name: dep.Name,
			Type: retType,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				// p.Source is the structure of the current row, and we need ForeignKey from it and parse it along as on ArgName
				v := reflect.ValueOf(p.Source)

				if v.Kind() == reflect.Ptr {
					v = v.Elem()
				}

				keyValue := v.FieldByName(dep.ForeignKey)

				if keyValue.IsZero() {
					return nil, fmt.Errorf("missing foreignKey '%s' in entity '%s' structure", dep.ForeignKey, name)
				}

				// if this is a null value, test for null and get interface value is not null
				key := keyValue.Interface()
				if v, ok := key.(driver.Valuer); ok {
					if i, err := v.Value(); err != nil {
						key = i
					} else {
						return nil, nil
					}
				}

				if dep.ArgName != "" {
					args := map[string]interface{}{}

					args[dep.ArgName] = key

					res := make([]interface{}, 0)

					if iter, err := dep.Def.Def.Query(p.Context, args, []string{}, -1, 0); err == nil {
						for iter.Next() {
							if row, err := iter.Row(); err == nil {
								res = append(res, row)
							} else {
								return nil, err
							}
						}
					} else {
						log.Error().
							Str("err", err.Error()).
							Msgf("query sub entity '%s'", dep.Name)
						return nil, err
					}

					return res, nil
				}

				// if 1:1 relation only use get not listings, and return struct not list
				row, err := dep.Def.Def.Get(p.Context, key)
				if err != nil {
					return nil, err
				}

				return row, nil
			},
		}
	}

	return flds
}

// Generate entity schema
func entityFields(queryFields *graphql.Fields, mutationFields *graphql.Fields) {
	for _, name := range entity.List() {
		def, _ := entity.Find(name)

		args := makeSearchArguments(def)

		// make sure all lists also have these generic list controlling arguments
		args["_offset"] = &graphql.ArgumentConfig{
			Type:        graphql.Int,
			Description: "the offset into to list generated, defaults to 0",
		}
		args["_limit"] = &graphql.ArgumentConfig{
			Type:        graphql.Int,
			Description: "max number of elements found in a list (-1 = all), defaults to 25",
		}
		args["_order"] = &graphql.ArgumentConfig{
			Type:        graphql.NewList(graphql.String),
			Description: "max number of elements found in a list (-1 = all), defaults to 25",
		}

		// make return type
		queryRet, _ := makeQueryType(name)

		// make a search query, that returns a list of type
		(*queryFields)[name+"_list"] = &graphql.Field{
			Type: graphql.NewList(queryRet),
			Args: args,
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				res := make([]interface{}, 0)

				offset := 0
				limit := 25
				order := []string{}

				if v, ok := p.Args["_offset"]; ok {
					if o, ok := v.(int); ok {
						offset = o
					}
				}
				if v, ok := p.Args["_limit"]; ok {
					if l, ok := v.(int); ok {
						limit = l
					}
				}
				if v, ok := p.Args["_order"]; ok {
					if i, ok := v.([]interface{}); ok {
						for _, el := range i {
							if o, ok := el.(string); ok {
								order = append(order, o)
							}
						}
					}
				}
				if iter, err := def.Def.Query(p.Context, p.Args, order, limit, offset); err == nil {
					for iter.Next() {
						if row, err := iter.Row(); err == nil {
							res = append(res, row)
						} else {
							return nil, err
						}
					}
				} else {
					log.Error().
						Str("err", err.Error()).
						Msgf("query %s_list", name)
					return nil, err
				}

				return res, nil
			},
		}

		// Return a single one of entity type
		(*queryFields)[name+"_get"] = &graphql.Field{
			Type: queryRet,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.Int,
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if id, ok := p.Args["id"]; ok {
					return def.Def.Get(p.Context, id)
				}
				return nil, fmt.Errorf("missing 'id' argument in get for '%s'", name)
			},
		}

		if up, ok := def.Def.(entity.Updateable); ok {
			// this entity may be an updateable type, not just a query type
			updateFields := makeUpdateFields(def)
			if len(updateFields) > 0 {
				(*mutationFields)[name+"_insert"] = &graphql.Field{
					Type: queryRet,
					Args: graphql.FieldConfigArgument{
						"data": &graphql.ArgumentConfig{
							Type: graphql.NewInputObject(graphql.InputObjectConfig{
								Name:   fmt.Sprintf("%s_insert_data", name),
								Fields: updateFields,
							}),
						},
					},
					Resolve: func(p graphql.ResolveParams) (interface{}, error) {
						if data, ok := p.Args["data"]; ok {
							return up.Create(p.Context, data.(map[string]interface{}))
						}
						return nil, fmt.Errorf("missing 'data' argument in insert of '%s'", name)
					},
				}

				(*mutationFields)[name+"_update"] = &graphql.Field{
					Type: graphql.Boolean,
					Args: graphql.FieldConfigArgument{
						"id": &graphql.ArgumentConfig{
							Type: graphql.Int,
						},
						"data": &graphql.ArgumentConfig{
							Type: graphql.NewInputObject(graphql.InputObjectConfig{
								Name:   fmt.Sprintf("%s_update_data", name),
								Fields: updateFields,
							}),
						},
					},
					Resolve: func(p graphql.ResolveParams) (interface{}, error) {
						if data, ok := p.Args["data"]; ok {
							if id, ok := p.Args["id"]; ok {
								return up.Update(p.Context, id, data.(map[string]interface{})), nil
							}
							return nil, fmt.Errorf("missing 'id' argument in update of '%s'", name)
						}

						return nil, fmt.Errorf("missing 'data' argument in update of '%s'", name)
					},
				}
			}

			(*mutationFields)[name+"_delete"] = &graphql.Field{
				Type: graphql.Boolean,
				Args: graphql.FieldConfigArgument{
					"id": &graphql.ArgumentConfig{
						Type: graphql.Int,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					if id, ok := p.Args["id"]; ok {
						return up.Delete(p.Context, id), nil
					}

					return nil, fmt.Errorf("missing 'id' argument in delete of '%s'", name)
				},
			}
		}
	}
}

func schemaConfig() graphql.SchemaConfig {
	queryFields := graphql.Fields{}
	mutationFields := graphql.Fields{
		"login": &graphql.Field{
			Type: graphql.Boolean,
			Args: graphql.FieldConfigArgument{
				"login": &graphql.ArgumentConfig{
					Type:        graphql.String,
					Description: "login name",
				},
				"password": &graphql.ArgumentConfig{
					Type:        graphql.String,
					Description: "password for login",
				},
			},
			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				if sess := service.SessionGet(p.Context); sess != nil {
					if err := sess.Login(p.Context, map[string]string{
						"login":    p.Args["login"].(string),
						"password": p.Args["password"].(string),
					}); err != nil {
						return nil, err
					}
					log.Debug().Msgf("loggedin using session id %d", sess.ID)
					return true, nil
				}

				return false, nil
			},
		},
	}

	queryFields["ping"] = &graphql.Field{
		Type: graphql.String,
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			return "pong", nil
		},
	}

	status := graphql.NewObject(graphql.ObjectConfig{
		Name: "userStatus",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.Int,
			},
			"ident": &graphql.Field{
				Type: graphql.String,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
			"email": &graphql.Field{
				Type: graphql.String,
			},
		},
	})

	queryFields["status"] = &graphql.Field{
		Type: status,
		Args: graphql.FieldConfigArgument{},
		Resolve: func(p graphql.ResolveParams) (interface{}, error) {
			if sess := service.SessionGet(p.Context); sess != nil {
				st := struct {
					ID    uint64 `json:"id"`
					Ident string `json:"ident"`
					Name  string `json:"name"`
					Email string `json:"email"`
				}{}

				st.ID = sess.ID

				if v, ok := sess.Get("ident"); ok {
					st.Ident = v.(string)
				}
				if v, ok := sess.Get("name"); ok {
					st.Name = v.(string)
				}
				if v, ok := sess.Get("email"); ok {
					st.Email = v.(string)
				}

				return st, nil
			}
			return nil, fmt.Errorf("session is missing")
		},
	}

	// Add structure for entity fields
	entityFields(&queryFields, &mutationFields)

	rootQuery := graphql.ObjectConfig{
		Name:   "RootQuery",
		Fields: queryFields,
	}

	rootMutation := graphql.ObjectConfig{
		Name:   "RootMutation",
		Fields: mutationFields,
	}

	return graphql.SchemaConfig{
		Query:    graphql.NewObject(rootQuery),
		Mutation: graphql.NewObject(rootMutation),
	}
}

// Setup GraphQL in gin engine
func Setup(r chi.Router) {
	schema, err := graphql.NewSchema(schemaConfig())

	if err != nil {
		log.Fatal().Msgf("failed to create new schema, error: %v", err)
		return
	}

	h := handler.New(&handler.Config{
		Schema:   &schema,
		Pretty:   true,
		GraphiQL: true,
	})

	r.Handle("/", h)
}
