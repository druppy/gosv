package graphql

import (
	"database/sql"
	"strconv"
	"time"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/graphql/language/ast"
)

// NullString scalar type for sql null string
type NullString struct {
	sql.NullString
}

// newNullString create a new null string. Empty string evaluates to an
// "invalid" NullString
func newNullString(value string) *NullString {
	var null NullString
	if value != "" {
		null.String = value
		null.Valid = true
		return &null
	}
	null.Valid = false
	return &null
}

// NullableString graphql *Scalar type based of NullString
var NullableString = graphql.NewScalar(graphql.ScalarConfig{
	Name:        "NullableString",
	Description: "The `NullableString` type repesents a nullable SQL string.",
	Serialize: func(value interface{}) interface{} {
		switch value := value.(type) {
		case sql.NullString:
			if value.Valid {
				return value.String
			}
		case *sql.NullString:
			v := *value
			if v.Valid {
				return v.String
			}
		}
		return nil
	},
	ParseValue: func(value interface{}) interface{} {
		switch value := value.(type) {
		case string:
			return newNullString(value)
		case *string:
			return newNullString(*value)
		default:
			return nil
		}
	},
	ParseLiteral: func(valueAST ast.Value) interface{} {
		switch valueAST := valueAST.(type) {
		case *ast.StringValue:
			return newNullString(valueAST.Value)
		default:
			return nil
		}
	},
})

// NullInt64 is a special GraphQL version
type NullInt64 struct {
	sql.NullInt64
}

func newNullInt64(value string) *NullInt64 {
	var null NullInt64

	if i, err := strconv.ParseInt(value, 10, 64); err == nil {
		null.Int64 = i
		null.Valid = true
		return &null
	}
	null.Valid = false
	return &null
}

// NullableInt64 graphql *Scalar type based of NullString
var NullableInt64 = graphql.NewScalar(graphql.ScalarConfig{
	Name:        "NullableInt64",
	Description: "The `NullableInt64` type repesents a nullable SQL int64.",
	Serialize: func(value interface{}) interface{} {
		switch value := value.(type) {
		case sql.NullInt64:
			if value.Valid {
				return value.Int64
			}
		case *sql.NullInt64:
			v := *value
			if v.Valid {
				return v.Int64
			}
		}
		return nil
	},
	ParseValue: func(value interface{}) interface{} {
		switch value := value.(type) {
		case string:
			return newNullInt64(value)
		case *string:
			return newNullInt64(*value)
		default:
			return nil
		}
	},
	ParseLiteral: func(valueAST ast.Value) interface{} {
		switch valueAST := valueAST.(type) {
		case *ast.StringValue:
			return newNullInt64(valueAST.Value)
		default:
			return nil
		}
	},
})

// NullTime scalar type for sql null string
type NullTime struct {
	sql.NullTime
}

func newNullTime(value string) *NullTime {
	var null NullTime
	if tval, err := time.Parse(time.RFC3339, value); err == nil {
		null.Time = tval
		null.Valid = true
		return &null
	}
	null.Valid = false
	return &null
}

// NullableTime graphql *Scalar type based of NullString
var NullableTime = graphql.NewScalar(graphql.ScalarConfig{
	Name:        "NullableTime",
	Description: "The `NullableTime` type repesents a nullable SQL time.",
	Serialize: func(value interface{}) interface{} {
		switch value := value.(type) {
		case sql.NullTime:
			if value.Valid {
				return value.Time.Format(time.RFC3339)
			}
		case *sql.NullTime:
			v := *value
			if v.Valid {
				return v.Time.Format(time.RFC3339)
			}
		}
		return nil
	},
	ParseValue: func(value interface{}) interface{} {
		switch value := value.(type) {
		case string:
			return newNullTime(value)
		case *string:
			return newNullTime(*value)
		default:
			return nil
		}
	},
	ParseLiteral: func(valueAST ast.Value) interface{} {
		switch valueAST := valueAST.(type) {
		case *ast.StringValue:
			return newNullTime(valueAST.Value)
		default:
			return nil
		}
	},
})
