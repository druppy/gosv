package entity

import (
	"database/sql"
	"reflect"

	sqlbuilder "github.com/huandu/go-sqlbuilder"
)

type resultIter struct {
	rows       *sql.Rows
	eStruct    *sqlbuilder.Struct
	structType reflect.Type
	total      int64 // total count of rows for the query
}

// NewIterator created a new iterator type
func newIterator(rows *sql.Rows, total int64, eStruct *sqlbuilder.Struct, st interface{}) *resultIter {
	return &resultIter{
		rows:       rows,
		eStruct:    eStruct,
		structType: reflect.TypeOf(st),
		total:      total,
	}
}

func (r *resultIter) Next() bool {
	return r.rows.Next()
}

func (r *resultIter) Count() int64 {
	return r.total
}

func (r *resultIter) Row() (interface{}, error) {
	res := reflect.New(r.structType).Interface()

	if r.structType.Kind() == reflect.Ptr {
		res = reflect.ValueOf(res).Elem().Interface()
	}

	if err := r.rows.Scan(r.eStruct.Addr(res)...); err != nil {
		return nil, err
	}

	return res, nil
}

func (r *resultIter) Close() {
	if r.rows != nil {
		r.rows.Close()
	}
}
