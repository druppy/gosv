package entity

/**
We define a structure for the entity to return that have several functions

it define the return structure for listing, and it holds 3 kinds of tags

db : define the real name of the database field and for Graphql name too
desc : define any description text for the graphql definitions
fieldtag : when set to 'update' it will be used for update arguments (see go-sqlbuilder)
*/

import (
	"context"
	"reflect"
)

// Arg define a single argument
type Arg struct {
	Name string
	Type reflect.Type
	Desc string
}

// QueryResult holds Query result, like a DBqueryFields := entityFields()
type QueryResult interface {
	Next() bool
	Count() int64
	Row() (interface{}, error)
}

// Query defines an interface for readonly Entity's
type Query interface {
	Get(ctx context.Context, key interface{}) (interface{}, error)
	Query(ctx context.Context, args map[string]interface{}, order []string, limit int, offset int) (QueryResult, error)
}

// Update able defines an update able interface
type Updateable interface {
	Query
	Create(ctx context.Context, args map[string]interface{}) (interface{}, error)
	Update(ctx context.Context, key interface{}, data map[string]interface{}) bool
	Delete(ctx context.Context, key interface{}) bool
}

// Dependency define a entity definition for complex queries
type Dependency struct {
	Name       string
	Def        *Definition
	ForeignKey string // where to get the key for this entity
	ArgName    string // the name of the argument for 1:n (if omittet we use key and 1:1)
}

type EntitySetup func(*Definition) error

// Definition holds a entity
type Definition struct {
	Cols  reflect.Type // Columns as a structure type
	Args  []Arg        // Arguments as a structure type
	Deps  []Dependency
	Def   Query
	setup EntitySetup
}

func (def *Definition) DependencyAdd(EntityName string, foreignKey string, argName string) error {
	d, err := Find(EntityName)
	if err != nil {
		return err
	}

	def.Deps = append(def.Deps, Dependency{
		Name:       EntityName,
		Def:        d,
		ForeignKey: foreignKey,
		ArgName:    argName,
	})

	return nil
}
