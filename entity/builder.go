package entity

// Common entity table handling helper, handling most common SQL work
// When this work as intended it will be moved to gosv
import (
	"context"
	"database/sql"
	"fmt"
	"reflect"
	"strings"

	"github.com/huandu/go-sqlbuilder"
	"github.com/rs/zerolog/log"
	"gitlab.com/druppy/gosv/service"
)

// QueryBuilder builder relevant clauses
type QueryBuilder interface {
	GetKey(cond *sqlbuilder.Cond, key interface{}) []string
	Query(cond *sqlbuilder.Cond, args map[string]interface{}) []string
}

// SetupBuilder allow for a setup of all select statements
type SetupBuilder interface {
	QueryBuilder
	Setup(sb *sqlbuilder.SelectBuilder)
}

type commonQuery struct {
	name        string
	native      interface{}
	qb          QueryBuilder
	buildStruct *sqlbuilder.Struct
}

type commonUpdate struct {
	commonQuery
}

/*
	RegQuery register an entity query handler based on a few user given primitives

 	- tableName is the name of the table in the DB
 	- cols contains the struct layout of any return types
 	- args is a user defined interface for argument handling
 	- setup is a setup function called when all entities are added to enable things like relations
*/
func RegQuery(tableName string, cols interface{}, args QueryBuilder, setup EntitySetup) error {
	colsType := reflect.TypeOf(cols)

	if colsType.Kind() == reflect.Ptr {
		return fmt.Errorf("column structure must be an instance, not a pointer")
	}

	if reflect.TypeOf(args).Kind() == reflect.Ptr {
		return fmt.Errorf("query builder must be an instance, not a pointer")
	}

	return Reg(tableName, &Definition{
		Cols: colsType,
		Args: MakeArgs(args),
		Def: &commonQuery{
			name:   tableName,
			native: cols,
			qb:     args,
		},
		setup: setup,
	})
}

/*
	RegUpdate is the same as RegQuery, except it allows for updating the main table

	@param tableName is the name of the table in the DB
	@param cols contains the struct layout of any return types
	@param args is a user defined interface for argument handling
	@param setup is a setup function called when all entities are added to enable things like relations
*/
func RegUpdate(tableName string, cols interface{}, args QueryBuilder, setup EntitySetup) error {
	colsType := reflect.TypeOf(cols)

	if colsType.Kind() == reflect.Ptr {
		return fmt.Errorf("column structure must be an instance, not a pointer")
	}

	if reflect.TypeOf(args).Kind() == reflect.Ptr {
		return fmt.Errorf("query builder must be an instance, not a pointer")
	}

	return Reg(tableName, &Definition{
		Cols: colsType,
		Args: MakeArgs(args),
		Def: &commonUpdate{
			commonQuery{
				name:   tableName,
				native: cols,
				qb:     args,
			},
		},
		setup: setup,
	})
}

// MakeArgs take a structure and return it as entity arg, to allow for usage of argument structures
func MakeArgs(argStruct interface{}) []Arg {
	var args []Arg

	s := reflect.TypeOf(argStruct)

	if s.Kind() == reflect.Ptr {
		s = s.Elem()
	}

	for i := 0; i < s.NumField(); i++ {
		el := s.Field(i)

		args = append(args, Arg{
			Name: el.Name,
			Type: el.Type,
		})
	}

	return args
}

func (e *commonQuery) selectBuilder() *sqlbuilder.SelectBuilder {
	if e.buildStruct == nil {
		e.buildStruct = sqlbuilder.NewStruct(e.native)
	}

	sb := e.buildStruct.SelectFrom(e.name)

	if update, ok := e.qb.(SetupBuilder); ok {
		update.Setup(sb)
	}

	return sb
}

func (e *commonQuery) newIterator(rows *sql.Rows, total int64) *resultIter {
	return newIterator(rows, total, e.buildStruct, e.native)
}

// fulfill Query interface
func (e *commonQuery) Get(ctx context.Context, key interface{}) (interface{}, error) {
	sess := service.SessionGet(ctx)

	sb := e.selectBuilder()
	sb.Where(e.qb.GetKey(&sb.Cond, key)...)

	sql, args := sb.Build()
	tx, err := sess.GetTx(ctx, "default")
	if err == nil {
		log.Debug().
			Str("SQL", sql).
			Msgf("user query")

		rows, err := tx.Query(sql, args...)
		if err == nil {
			var res = e.newIterator(rows, 1)
			defer res.Close()

			if res.Next() {
				return res.Row()
			}

			return nil, fmt.Errorf("missing data from entity '%s', using key values %v", e.name, key)
		}
		return nil, err
	}
	return nil, err
}

func (e *commonQuery) Query(ctx context.Context, args map[string]interface{}, order []string, limit int, offset int) (QueryResult, error) {
	sess := service.SessionGet(ctx)

	sb := e.selectBuilder()
	sb.Where(e.qb.Query(&sb.Cond, args)...)

	sb.Limit(limit)
	sb.Offset(offset)

	if len(order) > 0 {
		sb.OrderBy(order...)
	}

	sql, dbArgs := sb.Build()
	tx, err := sess.GetTx(ctx, "default")
	if err == nil {
		var total int64 = -1

		// Get the total count too, if limit or offset is set
		if offset == -1 || limit == -1 {
			sb.Select(sb.As("count(*)", "total"))
			csql, cargs := sb.Build()

			log.Debug().
				Str("SQL", csql).
				Msgf("user count query")

			if err := tx.QueryRowx(csql, cargs...).Scan(&total); err != nil {
				return nil, err
			}
		}

		log.Debug().
			Str("SQL", sql).
			Msgf("user query")

		rows, err := tx.Query(sql, dbArgs...)
		if err == nil {
			var res = e.newIterator(rows, total)

			return res, nil
		}
		return nil, err
	}
	return nil, err
}

// colDBtag returns the db tag from cols, if cound else the name
func colDBtag(cols interface{}, name string) string {
	colsType := reflect.TypeOf(cols)

	if colsType.Kind() == reflect.Struct {
		if fld, ok := colsType.FieldByName(name); ok {
			if v, ok := fld.Tag.Lookup("db"); ok {
				return v
			}

		}
	}

	return name
}

func (e *commonUpdate) Create(ctx context.Context, args map[string]interface{}) (interface{}, error) {
	var cols []string
	var vals []interface{}

	for arg, val := range args {
		cols = append(cols, colDBtag(e.native, arg))
		vals = append(vals, val)
	}

	ib := sqlbuilder.NewInsertBuilder().
		InsertInto(e.name).
		Cols(cols...).
		Values(vals)

	stmt, dbArgs := ib.Build()
	cols = append(cols, "id")
	stmt += " RETURNING " + strings.Join(cols, ",")
	sess := service.SessionGet(ctx)
	tx, err := sess.GetTx(ctx, "default")
	if err == nil {
		log.Debug().
			Str("SQL", stmt).
			Msgf("insert")

		rows, err := tx.Query(stmt, dbArgs...) // Using Query to allow for returning
		if err != nil {
			return nil, err
		}

		i := e.newIterator(rows, 1)
		defer i.Close()
		if i.Next() {
			return i.Row()
		}
		return nil, fmt.Errorf("missing return values from INSERT")
	}

	return nil, err
}

func (e *commonUpdate) Update(ctx context.Context, key interface{}, data map[string]interface{}) bool {
	ub := sqlbuilder.NewUpdateBuilder().
		Update(e.name)

	ub.Where(e.qb.GetKey(&ub.Cond, key)...)

	var assigns []string
	for n, val := range data {
		assigns = append(assigns, ub.Assign(colDBtag(e.native, n), val))
	}

	ub.Set(assigns...)

	sess := service.SessionGet(ctx)

	stmt, dbArgs := ub.Build()
	tx, err := sess.GetTx(ctx, "default")
	if err == nil {
		log.Debug().
			Str("SQL", stmt).
			Msgf("update")

		res, err := tx.Exec(stmt, dbArgs...) // Using Query to allow for returning
		if err != nil {
			log.Error().
				Str("SQL", stmt).
				Msg(err.Error())

			return false
		}

		cnt, err := res.RowsAffected()
		if err != nil {
			log.Error().
				Str("SQL", stmt).
				Msg(err.Error())

			return false
		}

		if cnt != 1 {
			log.Error().
				Str("SQL", stmt).
				Msgf("wrong number of rows affected on update expected 1 but got %d", cnt)

			return false
		}

		return true
	}
	return false
}

func (e *commonUpdate) Delete(ctx context.Context, key interface{}) bool {
	db := sqlbuilder.NewDeleteBuilder().
		DeleteFrom(e.name)

	db.Where(e.qb.GetKey(&db.Cond, key)...)

	stmt, dbArgs := db.Build()
	sess := service.SessionGet(ctx)
	tx, err := sess.GetTx(ctx, "default")
	if err != nil {
		return false
	}
	res, err := tx.Exec(stmt, dbArgs...) // Using Query to allow for returning
	if err != nil {
		log.Error().
			Str("SQL", stmt).
			Msg(err.Error())

		return false
	}

	cnt, err := res.RowsAffected()
	if err != nil {
		log.Error().
			Str("SQL", stmt).
			Msg(err.Error())

		return false
	}

	if cnt != 1 {
		log.Error().
			Str("SQL", stmt).
			Msgf("wrong number of rows affected on delete expected 1 but got %d", cnt)

		return false
	}

	return false
}
