package entity

import (
	"fmt"

	"github.com/pkg/errors"
)

var definitions = make(map[string]*Definition)

// Reg a new entity
func Reg(name string, entity *Definition) error {
	if _, ok := definitions[name]; ok {
		return fmt.Errorf("entity '%s' already exists", name)
	}

	definitions[name] = entity
	return nil
}

// List Provide possible definition names
func List() []string {
	keys := make([]string, len(definitions))
	i := 0
	for k := range definitions {
		keys[i] = k
		i++
	}

	return keys
}

// Find an entity and return it if found
func Find(name string) (*Definition, error) {
	if v, ok := definitions[name]; ok {
		return v, nil
	}

	return nil, fmt.Errorf("entity definition '%s' not found", name)
}

func SetupAll() error {
	for k := range definitions {
		if def, ok := definitions[k]; ok {
			err := def.setup(def)

			if err != nil {
				return errors.Wrapf(err, "setup of entity definition '%s' failed", k)
			}
		}
	}
	return nil
}
