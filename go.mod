module gitlab.com/druppy/gosv

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/gorilla/securecookie v1.1.1
	github.com/graphql-go/graphql v0.7.9
	github.com/graphql-go/handler v0.2.3
	github.com/huandu/go-sqlbuilder v1.12.1
	github.com/jmoiron/sqlx v1.3.3
	github.com/lib/pq v1.10.1 // indirect
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.21.0
)

go 1.15
