// Basic event system, for simple loos internal copling

package service

// Event defines a structure send to all event handlers
type Event struct {
	name string
	data map[string]interface{}
}

// EventFn defines the function type used for event handlers
type EventFn func(ev Event)

var register = make(map[string][]EventFn)

// Reg add a new handler for a specific event
func Reg(name string, fn EventFn) {
	register[name] = append(register[name], fn)
}

// Post a new event to all listende on that event
func Post(ev Event) {
	v, ok := register[ev.name]

	if ok {
		for _, fn := range v {
			fn(ev)
		}
	}
}
