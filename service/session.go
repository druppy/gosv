package service

/**
This session module handle sessions related to the user. Each new user gets a secure session cookie,
that contain an sequence number and if the user has logged in an ID (normally an user id).

This means that the session cookie change content after login, and we trust the content of the cookie,
as it is entrypted and hashed.

Internally we create aa session context per request, that points to a session and map of open database
connections. By having it this way, each connection attempt is done in its own trasaction scope to keep
data integrity high.
*/

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/render"
	"github.com/gorilla/securecookie"
	"github.com/jmoiron/sqlx"
)

const cookieName = "sess_id"

type contextKey string

const sessionKey contextKey = "__session" // maps to the current users session context

// https://stackoverflow.com/questions/22892120/how-to-generate-a-random-string-of-a-fixed-length-in-go
func randStr(len int) string {
	buff := make([]byte, len)
	rand.Read(buff)
	str := base64.StdEncoding.EncodeToString(buff)
	// Base 64 can be longer than len
	return str[:len]
}

var databases = map[string]*sqlx.DB{}

// SetDB set database connection for all future sessions
func SetDB(name string, db *sqlx.DB) {
	databases[name] = db
}

// Session for service
type Session struct {
	ID      uint64
	store   map[string]interface{} // dispoable values
	factory *SessionFactory
}

// internal session context structure
type contextSession struct {
	sess *Session
	w    http.ResponseWriter
	txs  map[string]*sqlx.Tx // really need to go to context
}

func sessionDBNew(r *http.Request, w http.ResponseWriter, sess *Session) context.Context {
	tx := &contextSession{
		sess: sess,
		w:    w,
		txs:  make(map[string]*sqlx.Tx, 0),
	}

	return context.WithValue(r.Context(), sessionKey, tx)
}

func (ctx *contextSession) txGet(name string) (*sqlx.Tx, error) {
	if tx, ok := ctx.txs[name]; ok {
		return tx, nil
	}

	if d, ok := databases[name]; ok {
		tx, err := d.Beginx()
		if err != nil {
			return nil, err
		}

		ctx.txs[name] = tx
		return tx, nil
	}

	return nil, fmt.Errorf("missing database name %s", name)
}

func (ctx *contextSession) Commit() {
	if ctx.txs != nil {
		for _, tx := range ctx.txs {
			tx.Commit()
		}
	}
}

// SessionGet fetch session related to request
func sessionContextGet(ctx context.Context) *contextSession {
	if ctx, ok := ctx.Value(sessionKey).(*contextSession); ok {
		return ctx
	}
	return nil
}

// LoginArgs holds data for login
type LoginArgs map[string]string

// Login is a special function that deligate login to the factory login handler
func (sess *Session) Login(ctx context.Context, args LoginArgs) error {
	if sess.factory != nil {
		id, err := sess.factory.login(ctx, sess, args)
		if err != nil {
			return err
		}

		sess.ID = id

		if c := sessionContextGet(ctx); c != nil {
			if err := sess.factory.update(sess, c.w); err != nil {
				return err
			}
		} else {
			return fmt.Errorf("missing internal session context in context")
		}
	}
	return nil
}

// SetID set special id, the defined a valid user in the system
func (sess *Session) SetID(ID uint64) {
	sess.ID = ID
}

// Get session value
func (sess *Session) Get(key string) (interface{}, bool) {
	v, ok := sess.store[key]

	if ok {
		return v, true
	}
	return nil, false
}

// Set a session value
func (sess *Session) Set(key string, value interface{}) {
	sess.store[key] = value
}

// GetTx will return a transaction for a named database commection
func (sess *Session) GetTx(ctx context.Context, name string) (*sqlx.Tx, error) {
	if c := sessionContextGet(ctx); c != nil {
		return c.txGet(name)
	}

	return nil, fmt.Errorf("internal context is missing session context")
}

// Commit any database transaction given
func (sess *Session) Commit(ctx context.Context) {
	if c := sessionContextGet(ctx); c != nil {
		c.Commit()
	}
}

type initFn = func(ctx context.Context, sess *Session) error
type loginFn = func(ctx context.Context, sess *Session, args LoginArgs) (uint64, error)

// SessionFactory generater of sessions
type SessionFactory struct {
	secure *securecookie.SecureCookie
	cache  map[string]*Session
	init   initFn  // called every time a session has been created
	login  loginFn // Called on user login
}

// SessionFactoryNew creates a new session factory
func SessionFactoryNew(hashKey string, blockKey string) *SessionFactory {
	s := SessionFactory{
		secure: securecookie.New([]byte(hashKey), []byte(blockKey)),
		cache:  make(map[string]*Session),
	}
	return &s
}

// InitSet will setup a init function for the factory
func (f *SessionFactory) InitSet(fn initFn) *SessionFactory {
	f.init = fn
	return f
}

// LoginSet will setup an login function
func (f *SessionFactory) LoginSet(fn loginFn) *SessionFactory {
	f.login = fn
	return f
}

// SessionGet fetch session related to request
func SessionGet(ctx context.Context) *Session {
	if ctxSess, ok := ctx.Value(sessionKey).(*contextSession); ok {
		return ctxSess.sess
	}
	return nil
}

// Find will extract the secure cookie, and make a session from this
func (f *SessionFactory) Find(w http.ResponseWriter, r *http.Request) (*Session, error) {
	cookie, err := r.Cookie(cookieName)

	if err == http.ErrNoCookie {
		return nil, nil
	} else if err == nil {
		value := map[string]string{}

		if err = f.secure.Decode(cookieName, cookie.Value, &value); err == nil {
			if id, err := strconv.ParseUint(value["ID"], 10, 64); err == nil {
				seq := value["__seq"]

				if _, ok := f.cache[seq]; !ok {
					sess := &Session{
						ID: id,
						store: map[string]interface{}{
							"__seq": seq,
						},
						factory: f,
					}

					f.cache[seq] = sess

					// lib user may need to do some stuff on the session befor use
					if f.init != nil {
						ctx := sessionDBNew(r, w, sess)

						if err := f.init(ctx, sess); err != nil {
							return nil, err
						}
					}

					return sess, nil
				}

				return f.cache[seq], nil
			}
		}
	}

	return nil, err
}

// New will make a new cookie, and set it on the response amd return a session object
func (f *SessionFactory) New(w http.ResponseWriter, r *http.Request) (*Session, error) {
	seq := randStr(20)

	sess := &Session{
		ID: 0,
		store: map[string]interface{}{
			"__seq": seq,
		},
		factory: f,
	}

	f.cache[seq] = sess

	// lib user may need to do some stuff on the session befor use
	if f.init != nil {
		ctx := sessionDBNew(r, w, sess)

		f.init(ctx, sess)
	}

	f.update(sess, w)

	return sess, nil
}

// Update secure user cookie to reflect session content
func (f *SessionFactory) update(sess *Session, w http.ResponseWriter) error {
	if seq, ok := sess.Get("__seq"); ok {
		value := map[string]string{
			"__seq": seq.(string),
		}

		if sess.ID > 0 {
			value["ID"] = strconv.FormatUint(sess.ID, 10)
		}

		encoded, err := f.secure.Encode(cookieName, &value)
		if err != nil {
			return err
		}

		cookie := &http.Cookie{
			Name:  cookieName,
			Value: encoded,
			Path:  "/",
		}

		http.SetCookie(w, cookie)
	}
	return nil
}

// MiddlewareHandler will set and maintain session cookie for user
func (f *SessionFactory) MiddlewareHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		sess, err := f.Find(w, r)
		if err != nil {
			render.PlainText(w, r, fmt.Sprintf("%s", err))
			http.Error(w, http.StatusText(400), 400)
			return
		}

		if sess == nil {
			sess, err = f.New(w, r)
			if err != nil {
				render.PlainText(w, r, fmt.Sprintf("%s", err))
				http.Error(w, http.StatusText(400), 400)
				return
			}
		}

		ctx := sessionDBNew(r, w, sess)
		next.ServeHTTP(w, r.WithContext(ctx))
		defer sess.Commit(ctx) // make sure to commit
	})
}
