package service

/**
Simple session system that will hold data relevant to the current user state
*/

import (
	"net/http"
	"net/http/httputil"
	"net/url"
)

// ReverseProxy to URL, Note this makes go routines per cennection
func ReverseProxy(url *url.URL) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		// create the reverse proxy
		proxy := httputil.NewSingleHostReverseProxy(url)

		// Update the headers to allow for SSL redirection
		req.URL.Host = url.Host
		req.URL.Scheme = url.Scheme
		req.Header.Set("X-Forwarded-Host", req.Header.Get("Host"))
		req.Host = url.Host

		// Note that ServeHttp is non blocking and uses a go routine under the hood
		proxy.ServeHTTP(w, req)
	}
}
