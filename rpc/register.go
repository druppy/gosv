package rpc

import (
	"context"
	"fmt"
	"reflect"
)

// Define common RPC functions

type rpcType struct {
	name        string
	needContext bool
	fv          reflect.Value
	args        []reflect.Type
	ret         []reflect.Type
}

// make array of interfaces that really have the type of the function arguments
func (t *rpcType) makeArgs() []interface{} {
	var args []interface{}

	for _, at := range t.args {
		args = append(args, reflect.Zero(at).Interface())
	}

	return args
}

func (t *rpcType) argsIn(idx int) reflect.Value {
	return reflect.New(t.args[idx])
}

func (t *rpcType) argsLen() int {
	l := len(t.args)

	if t.needContext {
		l++
	}

	return l
}

var rpcList map[string]rpcType
var ctxType = reflect.TypeOf((*context.Context)(nil)).Elem()

func reflectFunc(name string, fptr interface{}) (*rpcType, error) {
	fnt := reflect.ValueOf(fptr).Type()

	if fnt.Kind() == reflect.Func {
		res := rpcType{
			name: name,
			fv:   reflect.ValueOf(fptr),
			args: make([]reflect.Type, 0),
			ret:  make([]reflect.Type, 0),
		}

		// Get arguments
		for i := 0; i < fnt.NumIn(); i++ {
			a := fnt.In(i)

			if i == 0 {
				if a.Implements(ctxType) {
					res.needContext = true
					continue
				}
			}

			res.args = append(res.args, a)
		}

		// get return value, if one is found
		if fnt.NumOut() > 1 {
			return nil, fmt.Errorf("RPC function can only handle one return value")
		} else if fnt.NumOut() == 1 {
			res.ret = append(res.ret, fnt.Out(0))
		}

		return &res, nil
	}

	return nil, fmt.Errorf("interface is not a function type")
}

// Reg register a normal go function as a RPC
func Reg(name string, fptr interface{}) error {
	if rpcList == nil {
		rpcList = make(map[string]rpcType, 0)
	}

	f, err := reflectFunc(name, fptr)
	if err != nil {
		return err
	}
	rpcList[f.name] = *f
	return nil
}
