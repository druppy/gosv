package rpc

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"strings"

	"github.com/go-chi/chi"
	"github.com/rs/zerolog/log"
	"gitlab.com/druppy/gosv/service"
)

const (
	rpcErrParse          = -32700 // Invalid JSON was received by the server.
	rpcErrInvalidRequest = -32600 // The JSON sent is not a valid Request object.
	rpcErrMethodNotFound = -32601 // Method not found
	rpcErrInvalidParams  = -32602 // Invalid method parameter(s).
	rpcErrInternal       = -32603 // Internal JSON-RPC error.
	rpcErrBegin          = -32000
	rpcErrEnd            = -32099 // Server errors Reserved for implementation-defined server-errors.
)

type rpcEnvelope struct {
	ID      int             `json:"id,omitempty"` // no id means this is an notification
	Version string          `json:"jsonrpc"`      // must be "2.0"
	Method  string          `json:"method"`
	Params  json.RawMessage `json:"params,omitempty"`
}

type rpcError struct {
	Code    int             `json:"code"`              // error code
	Message string          `json:"message,omitempty"` // textual message
	Data    json.RawMessage `json:"data,omitempty"`    // custom data related to error
}

type rpcResult struct {
	ID      int             `json:"id"`
	Version string          `json:"jsonrpc"`
	Result  json.RawMessage `json:"result,omitempty"`
	Error   *rpcError       `json:"error,omitempty"`
}

func makeRPCErrorResult(id int, code int, msg string) []byte {
	raw, _ := json.Marshal(rpcResult{
		ID:      id,
		Version: "2.0",
		Error: &rpcError{
			Code:    code,
			Message: msg,
		},
	})

	return raw
}

func makeRPCResult(id int, res interface{}) []byte {
	rawRes, _ := json.Marshal(res)

	raw, _ := json.Marshal(rpcResult{
		ID:      id,
		Version: "2.0",
		Result:  rawRes,
	})

	return raw
}

// Handler gets raw json data and try to parse it as JsonRPC 2.0, it will look for the function in the regitser
// hand pack the needed types for arguments for this function
// If the function takes a context as first argument, this will be taken from the handler argument
func Handler(ctx context.Context, msg []byte) []byte {
	var env rpcEnvelope

	if err := json.Unmarshal(msg, &env); err != nil {
		return makeRPCErrorResult(0, rpcErrParse, err.Error())
	}

	// Validate
	if env.Version != "2.0" {
		return makeRPCErrorResult(env.ID, rpcErrInvalidRequest, fmt.Sprintf("rpc version must be 2.0 and not '%s'", env.Version))
	}

	method, found := rpcList[env.Method]
	if !found {
		return makeRPCErrorResult(env.ID, rpcErrMethodNotFound, fmt.Sprintf("method '%s' is not found on server", env.Method))
	}

	// pack arguments for the actual call
	valArgs := []reflect.Value{}
	if method.needContext {
		valArgs = append(valArgs, reflect.ValueOf(ctx))
	}

	if env.Params != nil {
		// Get argument array
		args := []json.RawMessage{}

		// now parse these arguments for the function call
		if err := json.Unmarshal(env.Params, &args); err != nil {
			return makeRPCErrorResult(env.ID, rpcErrInvalidParams,
				fmt.Sprintf("argument parsing error for method '%s' : %s", env.Method, err))
		}

		if len(args) > method.argsLen() {
			return makeRPCErrorResult(env.ID, rpcErrInvalidParams,
				fmt.Sprintf("expect %d arguments, but got %d on method %s", method.argsLen(), len(args), env.Method))
		}

		// convert each arg as json (we use it as a tuple)
		for n, arg := range args {
			v := method.argsIn(n)

			if err := json.Unmarshal(arg, v.Interface()); err != nil {
				return makeRPCErrorResult(env.ID, rpcErrInvalidParams,
					fmt.Sprintf("argument type error for method '%s' : %s", env.Method, err))
			}
			valArgs = append(valArgs, v.Elem())
		}
	}

	// Make sure we have what we need
	if len(valArgs) != method.argsLen() {
		return makeRPCErrorResult(env.ID, rpcErrInvalidParams,
			fmt.Sprintf("method '%s' needs %d arguments but got %d", env.Method, method.argsLen(), len(valArgs)))
	}

	// at last ... call the registered function
	valRes := method.fv.Call(valArgs)
	if len(valRes) == 1 {
		val := valRes[0].Interface()

		if e, ok := val.(error); ok {
			makeRPCErrorResult(env.ID, rpcErrInternal, e.Error())
		}
		return makeRPCResult(env.ID, val)
	}

	// return results
	var resArray []interface{}
	for _, v := range valRes {
		val := v.Interface()
		if e, ok := val.(error); ok {
			makeRPCErrorResult(env.ID, rpcErrInternal, e.Error())
		}
		resArray = append(resArray, val)
	}

	return makeRPCResult(env.ID, resArray)
}

// Setup a middleware handler
func Setup(r chi.Router) {
	r.Post("/rpc", func(w http.ResponseWriter, r *http.Request) {
		if strings.Contains(r.Header.Get("Content-Type"), "application/json") {
			if reader, err := r.GetBody(); err == nil {
				body := make([]byte, r.ContentLength)
				reader.Read(body)
				w.Write(Handler(r.Context(), body))
			} else {
				http.Error(w, err.Error(), 500)
			}
		} else {
			http.Error(w, "json rpc must be application/json type", 500)
		}
	})

	// expose an rpc user login function too
	Reg("user_login", func(ctx context.Context, username string, password string) bool {
		if sess := service.SessionGet(ctx); sess != nil {
			if err := sess.Login(ctx, map[string]string{
				"login":    username,
				"password": password,
			}); err != nil {
				return false
			}
			log.Debug().Msgf("loggedin using session id %d", sess.ID)
			return true
		}

		return false
	})

	Reg("ping", func(ctx context.Context) string {
		if sess := service.SessionGet(ctx); sess != nil {
			// We may need to update the cookie timeout here !
			return "pong"
		}

		return ""
	})

}
