package rpc

import (
	"context"
	"encoding/json"
	"reflect"
	"testing"
)

func TestHandler(t *testing.T) {
	type args struct {
		ctx context.Context
		msg []byte
	}

	// Setup RPC functions for testing
	Reg("simple", func() bool { return true })
	Reg("adder", func(a, b int) int {
		return a + b
	})
	Reg("adder_ctx", func(ctx context.Context, a, b int) int {
		return a + b
	})
	Reg("array_adder", func(arr []int) int {
		var sum = 0
		for _, a := range arr {
			sum += a
		}
		return sum
	})
	Reg("struct_adder", func(arr struct {
		A int
		B int
	}) int {
		return arr.A + arr.B
	})

	tests := []struct {
		name string
		args args
		want json.RawMessage
		code int
	}{
		// TODO: Add test cases.
		{
			"simple",
			args{
				context.Background(),
				[]byte(`{"id":42,"jsonrpc":"2.0","method":"simple"}`),
			},
			json.RawMessage(`true`),
			0,
		},
		{
			"simple_bad_json",
			args{
				context.Background(),
				[]byte(`abcd`),
			},
			json.RawMessage(`true`),
			-32700,
		},
		{
			"simple_too_many_args",
			args{
				context.Background(),
				[]byte(`{"id":42,"jsonrpc":"2.0","method":"simple","params":[1,2]}`),
			},
			json.RawMessage(`true`),
			-32602,
		},
		{
			"number_adding",
			args{
				context.Background(),
				[]byte(`{"id":42,"jsonrpc":"2.0","method":"adder","params":[7,3]}`),
			},
			json.RawMessage(`10`),
			0,
		},
		{
			"number_adding_too_few_args",
			args{
				context.Background(),
				[]byte(`{"id":42,"jsonrpc":"2.0","method":"adder","params":[7]}`),
			},
			json.RawMessage(`10`),
			-32602,
		},
		{
			"number_adding_ctx",
			args{
				context.Background(),
				[]byte(`{"id":42,"jsonrpc":"2.0","method":"adder_ctx","params":[40,2]}`),
			},
			json.RawMessage(`42`),
			0,
		},
		{
			"number_adding_array",
			args{
				context.Background(),
				[]byte(`{"id":42,"jsonrpc":"2.0","method":"array_adder","params":[[2,4,6,8]]}`),
			},
			json.RawMessage(`20`),
			0,
		},
		{
			"number_adding_struct",
			args{
				context.Background(),
				[]byte(`{"id":42,"jsonrpc":"2.0","method":"struct_adder","params":[{"a":5,"b":15}]}`),
			},
			json.RawMessage(`20`),
			0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := Handler(tt.args.ctx, tt.args.msg)

			var res rpcResult
			if err := json.Unmarshal(got, &res); err != nil {
				t.Fatalf("test result json error '%s'", err)
			}

			if res.Error != nil {
				if res.Error.Code == tt.code {
					return
				}

				t.Fatalf("return error code %d : '%s'", res.Error.Code, res.Error.Message)
			}

			if tt.code != 0 {
				t.Fatalf("expected error code %d but got none", tt.code)
			}

			if !reflect.DeepEqual(res.Result, tt.want) {
				t.Errorf("Handler() returned = '%s'\nbut want '%s'", res.Result, tt.want)
			}
		})
	}
}
